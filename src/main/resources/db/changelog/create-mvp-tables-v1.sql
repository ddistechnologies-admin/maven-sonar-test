--liquibase formatted sql

--changeset id:Prashant

DROP TABLE IF EXISTS login;

CREATE TABLE login (
  login_id int NOT NULL ,
  mobile_no varchar(25) default NULL,
  mail_id varchar(50) default NULL,
  mpin varchar(255) default NULL,
  otp varchar(255) default NULL,
  otp_expired_at datetime default NULL,
  locked enum('Y','N') default 'N',
  no_of_attempts int default NULL,
  created_date datetime default NULL,
  last_modified_date datetime default NULL,
  is_verified boolean default false,
  PRIMARY KEY (login_id)
) ;




DROP TABLE IF EXISTS ent_activity_type;


CREATE TABLE ent_activity_type (
  activity_type_id int NOT NULL,
  activity_name varchar(120) NOT NULL,
  created_by int NOT NULL,
  created_date datetime NOT NULL,
  modified_date datetime DEFAULT NULL,
  PRIMARY KEY (activity_type_id),
  UNIQUE KEY activity_name (activity_name),
  KEY created_by (created_by),
  CONSTRAINT ent_activity_type_ibfk_1 FOREIGN KEY (created_by) REFERENCES login (login_id)
) ;

--
-- Table structure for table ent_city
--

DROP TABLE IF EXISTS ent_city;


CREATE TABLE ent_city (
  city_id int NOT NULL,
  city_name varchar(255) NOT NULL,
  PRIMARY KEY (city_id),
  UNIQUE KEY city_name (city_name)
) ;


--
-- Table structure for table ent_state
--

DROP TABLE IF EXISTS ent_state;


CREATE TABLE ent_state (
  state_id int NOT NULL,
  state_name varchar(120) NOT NULL,
  PRIMARY KEY (state_id),
  UNIQUE KEY state_name (state_name)
) ;


--
-- Table structure for table ent_status
--

DROP TABLE IF EXISTS ent_status;


CREATE TABLE ent_status (
  status_id int NOT NULL,
  status varchar(120) NOT NULL,
  PRIMARY KEY (status_id),
  UNIQUE KEY status (status)
) ;

--
-- Table structure for table gender
--

DROP TABLE IF EXISTS gender;


CREATE TABLE gender (
  gender_id int NOT NULL,
  gender varchar(120) NOT NULL,
  PRIMARY KEY (gender_id),
  UNIQUE KEY gender (gender)
) ;


--
-- Table structure for table ent_address
--

DROP TABLE IF EXISTS ent_address;


CREATE TABLE ent_address (
  address_id int NOT NULL ,
  address1 varchar(255) NOT NULL,
  address2 varchar(255) NOT NULL,
  city int NOT NULL,
  state int NOT NULL,
  pincode int NOT NULL,
  landmark varchar(255) DEFAULT NULL,
  created_by int NOT NULL,
  created_date datetime NOT NULL,
  last_modified_by int DEFAULT NULL,
  last_modified_date datetime DEFAULT NULL,
  PRIMARY KEY (address_id,city),
  KEY city (city),
  KEY state (state),
  KEY created_by (created_by),
  KEY last_modified_by (last_modified_by),
  CONSTRAINT ent_address_ibfk_1 FOREIGN KEY (city) REFERENCES ent_city (city_id),
  CONSTRAINT ent_address_ibfk_2 FOREIGN KEY (state) REFERENCES ent_state (state_id),
  CONSTRAINT ent_address_ibfk_3 FOREIGN KEY (created_by) REFERENCES login (login_id),
  CONSTRAINT ent_address_ibfk_4 FOREIGN KEY (last_modified_by) REFERENCES login (login_id)
) ;

DROP TABLE IF EXISTS user;

CREATE TABLE user (
  user_id int NOT NULL,
  login_id int NOT NULL,
  first_name varchar(50) NOT NULL,
  last_name varchar(50) NOT NULL,
  middle_name varchar(50) DEFAULT NULL,
  display_name varchar(150) NOT NULL,
  dob date DEFAULT NULL,
  location varchar(255) DEFAULT NULL,
  perm_address_id int DEFAULT NULL,
  temp_address_id int DEFAULT NULL,
  alternate_phone_no decimal(10,0) DEFAULT NULL,
  created_by int NOT NULL,
  last_modified_by int DEFAULT NULL,
  created_date datetime NOT NULL,
  last_modified_date datetime DEFAULT NULL,
  status_id int NOT NULL,
  gender_id int NOT NULL,
  marital_status enum('Y','N') DEFAULT NULL,
  PRIMARY KEY (user_id,login_id),
  KEY login_id (login_id),
  KEY created_by (created_by),
  KEY last_modified_by (last_modified_by),
  KEY gender_id (gender_id),
  KEY user_ibfk_6 (perm_address_id),
  KEY user_ibfk_7 (temp_address_id),
  KEY user_ibfk_4 (status_id),
  CONSTRAINT user_ibfk_1 FOREIGN KEY (login_id) REFERENCES login (login_id),
  CONSTRAINT user_ibfk_2 FOREIGN KEY (created_by) REFERENCES login (login_id),
  CONSTRAINT user_ibfk_3 FOREIGN KEY (last_modified_by) REFERENCES login (login_id),
  CONSTRAINT user_ibfk_4 FOREIGN KEY (status_id) REFERENCES ent_status (status_id),
  CONSTRAINT user_ibfk_5 FOREIGN KEY (gender_id) REFERENCES gender (gender_id),
  CONSTRAINT user_ibfk_6 FOREIGN KEY (perm_address_id) REFERENCES ent_address (address_id),
  CONSTRAINT user_ibfk_7 FOREIGN KEY (temp_address_id) REFERENCES ent_address (address_id)
) ;
--
-- Table structure for table ent_event_type
--

DROP TABLE IF EXISTS ent_event_type;


CREATE TABLE ent_event_type (
  event_type_id int NOT NULL,
  event_type_name varchar(120) NOT NULL,
  created_by int NOT NULL,
  created_date datetime NOT NULL,
  modified_date datetime DEFAULT NULL,
  PRIMARY KEY (event_type_id),
  UNIQUE KEY event_type_name (event_type_name),
  KEY created_by (created_by),
  CONSTRAINT ent_event_type_ibfk_1 FOREIGN KEY (created_by) REFERENCES login (login_id)
) ;


--
-- Table structure for table ent_location
--

DROP TABLE IF EXISTS ent_location;


CREATE TABLE ent_location (
  id int NOT NULL,
  name varchar(120) NOT NULL,
  longitude  varchar(120) NOT NULL,
  lattitude  varchar(120) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY name (name)
) ;



--
-- Table structure for table ent_service_types
--

DROP TABLE IF EXISTS ent_service_types;


CREATE TABLE ent_service_types (
  service_type_id int NOT NULL,
  service_name varchar(120) NOT NULL,
  PRIMARY KEY (service_type_id),
  UNIQUE KEY service_name (service_name)
) ;



DROP TABLE IF EXISTS service_request;

CREATE TABLE service_request (
  sr_id int NOT NULL,
  short_name varchar(100) NOT NULL,
  description text NOT NULL,
  service_type_id int DEFAULT NULL,
  other_service_name varchar(255) DEFAULT NULL,
  location_id int NOT NULL,
  max_price decimal(6,2) DEFAULT NULL,
  min_price decimal(6,2) DEFAULT NULL,
  exp_service_date date DEFAULT NULL,
  request_status int NOT NULL,
  user_id int NOT NULL,
  created_date datetime NOT NULL,
  modified_date datetime DEFAULT NULL,
  PRIMARY KEY (sr_id),
  KEY location_id (location_id),
  KEY user_id (user_id),
  KEY service_request_ibfk_1 (service_type_id),
  CONSTRAINT service_request_ibfk_1 FOREIGN KEY (service_type_id) REFERENCES ent_service_types (service_type_id),
  CONSTRAINT service_request_ibfk_2 FOREIGN KEY (location_id) REFERENCES ent_location (id),
  CONSTRAINT service_request_ibfk_3 FOREIGN KEY (user_id) REFERENCES login (login_id)
);

DROP TABLE IF EXISTS service_request_attachments;

CREATE TABLE service_request_attachments (
  id int NOT NULL AUTO_INCREMENT,
  service_request_id int NOT NULL,
  document blob,
  path varchar(1000) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY service_request_attachments_ibfk_1 (service_request_id),
  CONSTRAINT service_request_attachments_ibfk_1 FOREIGN KEY (service_request_id) REFERENCES service_request (sr_id)
);


DROP TABLE IF EXISTS vendor_services;

CREATE TABLE vendor_services (
  id int NOT NULL AUTO_INCREMENT,
  vendor_id int NOT NULL,
  service_id int NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY vendor_id (vendor_id,service_id),
  KEY vendor_services_ibfk_2 (service_id),
  CONSTRAINT vendor_services_ibfk_1 FOREIGN KEY (vendor_id) REFERENCES login (login_id),
  CONSTRAINT vendor_services_ibfk_2 FOREIGN KEY (service_id) REFERENCES ent_service_types (service_type_id)
) ;

/*Table structure for table property_type */

DROP TABLE IF EXISTS property_type;

CREATE TABLE property_type (
  id int NOT NULL,
  name varchar(120) NOT NULL,
  PRIMARY KEY (id)
) ;

/*Table structure for table community_type */

DROP TABLE IF EXISTS community_type;

CREATE TABLE community_type (
  id int NOT NULL,
  name varchar(120) NOT NULL,
  PRIMARY KEY (id)
) ;

DROP TABLE IF EXISTS community;

CREATE TABLE community (
  community_id int NOT NULL AUTO_INCREMENT,
  community_name varchar(120) NOT NULL,
  community_type_id int NOT NULL,
  address_id int NOT NULL,
  created_by int NOT NULL,
  created_date datetime NOT NULL,
  last_modified_date datetime DEFAULT NULL,
  location int NOT NULL,
  PRIMARY KEY (community_id),
  KEY created_by (created_by),
  KEY community_type_id (community_type_id),
  KEY property_ibfk_1 (address_id),
  KEY property_ibfk_5 (location),
  CONSTRAINT community_ibfk_1 FOREIGN KEY (address_id) REFERENCES ent_address (address_id),
  CONSTRAINT community_ibfk_2 FOREIGN KEY (created_by) REFERENCES login (login_id),
  CONSTRAINT community_ibfk_3 FOREIGN KEY (community_type_id) REFERENCES community_type (id),
  CONSTRAINT community_ibfk_4 FOREIGN KEY (location) REFERENCES ent_location (id)
) ;
DROP TABLE IF EXISTS community_details;

CREATE TABLE community_details (
  community_block_id int NOT NULL AUTO_INCREMENT,
  community_id int not null,
  block_name varchar(120) NOT NULL,
  from_no int NOT NULL,
  to_no int NOT NULL,
  created_by int NOT NULL,
  created_date datetime NOT NULL,
  last_modified_date datetime DEFAULT NULL,
  location int NOT NULL,
  PRIMARY KEY (community_block_id),
  KEY created_by (created_by),
  CONSTRAINT uc_ibfk_1 FOREIGN KEY (created_by) REFERENCES login (login_id),
  CONSTRAINT uc_ibfk_2 FOREIGN KEY (community_id) REFERENCES community (community_id)
) ;
DROP TABLE IF EXISTS property;

CREATE TABLE property (
  property_id int NOT NULL AUTO_INCREMENT,
  property_name varchar(120) NOT NULL,
  property_type_id int NOT NULL,
  community_block_id int ,
  address_id int NOT NULL,
  created_by int NOT NULL,
  last_modified_by int NOT NULL,
  created_date datetime NOT NULL,
  last_modified_date datetime DEFAULT NULL,
  location int NOT NULL,
  primary_contact_user int,
  PRIMARY KEY (property_id),
  KEY created_by (created_by),
  KEY last_modified_by (last_modified_by),
  KEY property_type_id (property_type_id),
  KEY property_ibfk_1 (address_id),
  KEY property_ibfk_5 (location),
  CONSTRAINT property_ibfk_1 FOREIGN KEY (address_id) REFERENCES ent_address (address_id),
  CONSTRAINT property_ibfk_2 FOREIGN KEY (created_by) REFERENCES login (login_id),
  CONSTRAINT property_ibfk_3 FOREIGN KEY (last_modified_by) REFERENCES login (login_id),
  CONSTRAINT property_ibfk_4 FOREIGN KEY (property_type_id) REFERENCES property_type (id),
  CONSTRAINT property_ibfk_5 FOREIGN KEY (location) REFERENCES ent_location (id),
  CONSTRAINT property_ibfk_6 FOREIGN KEY (community_block_id) REFERENCES community_details (community_block_id)
) ;

CREATE TABLE user_property (
  user_property_id int NOT NULL AUTO_INCREMENT,
  property_id int NOT NULL,
  user_id int NOT NULL,
  residant_type int,
  from_date date not null,
  to_date date,
  notes text,
  is_active tinyint,
  PRIMARY KEY (user_property_id),
  CONSTRAINT user_property_ibfk_1 FOREIGN KEY (property_id) REFERENCES property (property_id),
  CONSTRAINT user_property_ibfk_2 FOREIGN KEY (user_id) REFERENCES login (login_id)
  );
  
DROP TABLE IF EXISTS vendor_service_requests;

CREATE TABLE vendor_service_requests (
  id int NOT NULL AUTO_INCREMENT,
  sr_id int NOT NULL,
  vendor_id int NOT NULL,
  price double NOT NULL,
  status int NOT NULL,
  approved_date datetime DEFAULT NULL,
  closed_date datetime DEFAULT NULL,
  price_type int NOT NULL,
  requested_date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  KEY vendor_id (vendor_id),
  KEY sr_id (sr_id),
  CONSTRAINT vendor_service_requests_ibfk_1 FOREIGN KEY (vendor_id) REFERENCES login (login_id),
  CONSTRAINT vendor_service_requests_ibfk_2 FOREIGN KEY (sr_id) REFERENCES service_request (sr_id)

) 